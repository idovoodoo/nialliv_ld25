package ld25;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Vector2f;

/**
 *
 * @author Steve
 */
public class Stone {
    private Animation stoneAnim;
    private int xPos;
    private int yPos;
    private int xPosStart;
    private int yPosStart;
    private int facing = 3; //1 = up, 2 = right, 3 = down, 4 = left
    private int damage;
    public Polygon stonePoly;
    public int maxDistance = 150;
    private int speed = 3;
    private Vector2f stoneVector;
    
    public Stone(int x, int y, Vector2f angle, int damage){
        this.xPos = x;
        this.yPos = y;
        this.xPosStart = x;
        this.yPosStart = y;
        this.facing = facing;
        this.damage = damage;
        this.stoneVector = angle;
    }
    
    public void init() throws SlickException{
        SpriteSheet stoneSheet = new SpriteSheet("data/stone-small.png", 17, 16);
        stoneAnim = new Animation();
        stoneAnim.setAutoUpdate(true);
        
        for(int frame = 0; frame < 4; frame++)
            stoneAnim.addFrame(stoneSheet.getSprite(frame, 0), 100);
        

        
        stonePoly = new Polygon(new float[]{
				this.xPos,this.yPos,
				this.xPos+16,this.yPos,
				this.xPos+16,this.yPos+16,
				this.xPos,this.yPos+16
                            }); 
        
    }
    
    public void destroy(){
        this.destroy();
    }
    
    public boolean update()
    {
        boolean status = false;
        //System.out.println(Math.round(this.stoneVector.getX()));
        //this.xPos++;
        this.xPos = this.xPos + Math.round(this.stoneVector.getX());
        this.yPos = this.yPos - Math.round(this.stoneVector.getY());
        stonePoly.setX(this.xPos);
        stonePoly.setY(this.yPos);
        
        if(yPosStart - yPos < maxDistance){             
        }
        else
            status = true;
            
        //status = true;
//        switch(facing){
//            case 0:
//                break;
//            case 1:
//                if(yPosStart - yPos < maxDistance){
//                    this.yPos = this.yPos - speed;
//                    stonePoly.setY(yPos);               
//                }
//                else
//                    status = true;
//                break;
//            case 2:
//                if(xPos - xPosStart < maxDistance){
//                    this.xPos = this.xPos + speed;
//                    stonePoly.setX(xPos);
//                }
//                else
//                    status = true;
//                break;
//            case 3:
//                if(yPos - yPosStart < maxDistance){
//                    this.yPos = this.yPos + speed;
//                    stonePoly.setY(yPos);
//                }
//                else
//                    status = true;
//                break;           
//            case 4:
//                if(xPosStart - xPos < maxDistance){
//                    this.xPos = this.xPos - speed;
//                    stonePoly.setX(xPos);
//                }
//                else
//                    status = true;
//                break;
//        }
        
        return status;
    }
    
    public Animation getAnimation(){
        return stoneAnim;
    }
    
    public void setX(int x){
        xPos = x;
    }
    
    public void setY(int y){
        yPos = y;
    }
    
    public int getX(){
        return xPos;
    }
    
    public int getY(){
        return yPos;
    }
}
