package ld25;

import java.util.ArrayList;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

/**
 * 
 * @author Some dude called freeaks wrote the original!
 */
public class BlockMap {
    public static TiledMap tmap;
    public static int mapWidth;
    public static int mapHeight;
    private int square[] = {1,1,47,1,47,47,1,47}; //square shaped tile
    public static ArrayList<Object> entities;

    public BlockMap(String ref) throws SlickException {
        entities = new ArrayList<Object>();
        tmap = new TiledMap(ref, "data");
        mapWidth = tmap.getWidth() * tmap.getTileWidth();
        mapHeight = tmap.getHeight() * tmap.getTileHeight();
        System.out.println("sorting map " + mapWidth + " " + mapHeight);
        for (int x = 0; x < tmap.getWidth(); x++) {
            for (int y = 0; y < tmap.getHeight(); y++) {
                int tileID = tmap.getTileId(x, y, 0);
                //tileID = tileID - 1;
                //System.out.println("tile ID = " + tileID);
                if (tileID == 1) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "collision-tile")
                    );
                    //System.out.println("collision added");
                }
                if (tileID == 2) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "tree-bottomleft")
                    );
                }
                if (tileID == 3) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "tree-bottom")
                    );
                }
                if (tileID == 4) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "tree-bottomright")
                    );
                }
                if (tileID == 10) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "foliage-full")                                        
                    );
                }
                if (tileID == 11) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "foliage-roundleft")
                    );
                }
                if (tileID == 12) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "foliage-roundright")
                    );
                }
                if (tileID == 25) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "tree-angle-bottomleft")
                    );
                }
                if (tileID == 26) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "tree-angle-bottomleft")
                    );
                }
                if (tileID == 34) {
                    entities.add(
                    new Block(x * 48, y * 48, square, "house-wall")
                    );
                }
            }
        }
    }
}
