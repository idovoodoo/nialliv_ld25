/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ld25;

import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author Steve
 */
public class MainMenuState extends BasicGameState{

    int stateID = 0;
    Image background = null;
    Image background2 = null;
    Image startGameOption = null;
    Image startGameOption2 = null;
    Sound selectFx = null;
    
    int menuState = 0;
    
    public MainMenuState(int state){
        stateID = state;
    }
    
    @Override
    public int getID() {
        return stateID;
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {        
        background = new Image("data/menu-bgd.jpg");
        background2 = new Image("data/menu-bgd-2.jpg");
        startGameOption = new Image("data/menu-button.png");
        startGameOption2 = new Image("data/menu-button-2.png");
        selectFx = new Sound("data/select.wav");
        
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        //g.drawString("Main Menu", 10, 50);
        //g.drawString("Play", 10, 100);
        if(menuState == 0){
            background.draw(0, 0);
            //g.drawString("Start", 280, 275);
            startGameOption.draw(280, 275);
        }
        else if(menuState == 1){
            background2.draw(0, 0);
            startGameOption.draw(280, 432);
        }
        
    }

    @Override
    public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
        Input input = container.getInput();
        int xPos = input.getMouseX();
        int yPos = input.getMouseY();
        
        if(menuState == 0){
            if((xPos > 280 && xPos < 360) && (yPos > 270 && yPos < 330)){
                if(input.isMouseButtonDown(0)){
                    selectFx.play();
                    menuState = 1;
                    
                }
            }
        }
        else if(menuState == 1){
            if((xPos > 280 && xPos < 360) && (yPos > 432 && yPos < 477)){
                if(input.isMouseButtonDown(0)){
                    selectFx.play();
                    sbg.enterState(Game.PLAYSTATE);
                }
            }
        }
    }
    
}
