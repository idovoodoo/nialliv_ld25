package ld25;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Polygon;

/**
 *
 * @author Steve
 */
public class Flower {
    private Animation flowerAnim;
    private int xPos;
    private int yPos;
    private int xPosStart;
    private int yPosStart;
    private int facing = 3; //1 = up, 2 = right, 3 = down, 4 = left
    private int damage;
    public Polygon flowerPoly;
    public int maxDistance = 150;
    private int speed = 3;
    
    public Flower(int x, int y, int facing, int damage){
        this.xPos = x;
        this.yPos = y;
        this.xPosStart = x;
        this.yPosStart = y;
        this.facing = facing;
        this.damage = damage;
    }
    
    public void init() throws SlickException{
        SpriteSheet flowerSheet = new SpriteSheet("data/flower-small.png", 17, 16);
        flowerAnim = new Animation();
        flowerAnim.setAutoUpdate(true);
        
        for(int frame = 0; frame < 4; frame++)
            flowerAnim.addFrame(flowerSheet.getSprite(frame, 0), 100);
        

        
        flowerPoly = new Polygon(new float[]{
				this.xPos,this.yPos,
				this.xPos+16,this.yPos,
				this.xPos+16,this.yPos+16,
				this.xPos,this.yPos+16
                            }); 
        
    }
    
    public void destroy(){
        this.destroy();
    }
    
    public boolean update()
    {
        boolean status = false;
        
        switch(facing){
            case 0:
                break;
            case 1:
                if(yPosStart - yPos < maxDistance){
                    this.yPos = this.yPos - speed;
                    flowerPoly.setY(yPos);               
                }
                else
                    status = true;
                break;
            case 2:
                if(xPos - xPosStart < maxDistance){
                    this.xPos = this.xPos + speed;
                    flowerPoly.setX(xPos);
                }
                else
                    status = true;
                break;
            case 3:
                if(yPos - yPosStart < maxDistance){
                    this.yPos = this.yPos + speed;
                    flowerPoly.setY(yPos);
                }
                else
                    status = true;
                break;           
            case 4:
                if(xPosStart - xPos < maxDistance){
                    this.xPos = this.xPos - speed;
                    flowerPoly.setX(xPos);
                }
                else
                    status = true;
                break;
        }
        
        return status;
    }
    
    public Animation getAnimation(){
        return flowerAnim;
    }
    
    public void setX(int x){
        xPos = x;
    }
    
    public void setY(int y){
        yPos = y;
    }
    
    public int getX(){
        return xPos;
    }
    
    public int getY(){
        return yPos;
    }
}
