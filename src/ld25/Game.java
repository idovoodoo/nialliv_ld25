/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ld25;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author Steve
 */
public class Game extends StateBasedGame{

    public static final int MENUSTATE = 0;
    public static final int PLAYSTATE = 1;
    public static final int LEVELFINISHSTATE = 2;
    public static final int WIDTH = 640;
    public static final int HEIGHT = 480;
    
    public Game(){
        super("Niall IV ~repentance~");
        this.addState(new MainMenuState(MENUSTATE));
        this.addState(new GamePlayState(PLAYSTATE));
        this.addState(new LevelFinishState(LEVELFINISHSTATE));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SlickException {
        AppGameContainer app = new AppGameContainer(new Game());
        app.setDisplayMode(WIDTH, HEIGHT, false);
        app.setShowFPS(false);
        app.start();
    }

    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        //this.getState(MENUSTATE).init(container, this);
        //this.getState(PLAYSTATE).init(container, this);
        this.enterState(MENUSTATE);
        
    }
}
