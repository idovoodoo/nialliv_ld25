package ld25;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.tiled.TiledMap;

/**
 *
 * @author Steve
 */
public class GamePlayState extends BasicGameState{

    //declare private vars
    private int stateID = 0;
    private static Villain villain; 
    private Camera camera;
    private Timer timer;
    private int flowerPower;
    //declare public vars
    public BlockMap map0;
    private NPC npcBoy1, npcBoy2, npcBoy3, npcBoy4, npcBoy5, npcBoy6, npcBoy7, npcBoy8, npcBoy9, npcBoy10;
    private Sound flowerHit;
    
    private int gameState = 0; //0 = normal, 1 = finished
    public ArrayList<NPC> npcArray = new ArrayList<>(); 
    
    public GamePlayState(int state){
        stateID = state;        
    }
        
    @Override
    public int getID() {
        return stateID;
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        container.setVSync(true);
        map0 = new BlockMap("data/map0.tmx");
        flowerHit = new Sound("data/flower-hit.wav");
        villain = new Villain(190, 100);
        villain.init();
        timer = new Timer();
        timer.schedule(new MoreFlowerPowerTask(),0 , 1 * 250);
        camera = new Camera(container, BlockMap.tmap);        
        setupNPCs();
    }
    
    private void setupNPCs() throws SlickException{
        npcBoy1 = new NPC(480, 384, 1, 10);
        npcBoy2 = new NPC(432, 768, 1, 10);
        npcBoy3 = new NPC(96, 480, 1, 10);
        npcBoy4 = new NPC(240, 912, 1, 10);
        npcBoy5 = new NPC(816, 1152, 1, 10);
        npcBoy6 = new NPC(864, 1200, 1, 10);
        npcBoy7 = new NPC(1200, 960, 1, 10);
        npcBoy8 = new NPC(1248, 960, 1, 10);
        npcBoy9 = new NPC(1296, 960, 1, 10);
        npcBoy10 = new NPC(1152, 576, 1, 10);
        npcBoy1.init();
        npcBoy2.init();
        npcBoy3.init();
        npcBoy4.init();
        npcBoy5.init();
        npcBoy6.init();
        npcBoy7.init();
        npcBoy8.init();
        npcBoy9.init();
        npcBoy10.init();
        npcArray.add(npcBoy1);
        npcArray.add(npcBoy2);
        npcArray.add(npcBoy3);
        npcArray.add(npcBoy4);
        npcArray.add(npcBoy5);
        npcArray.add(npcBoy6);
        npcArray.add(npcBoy7);
        npcArray.add(npcBoy8);
        npcArray.add(npcBoy9);
        npcArray.add(npcBoy10);
    }
    
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        camera.drawMap();
        camera.translateGraphics();
        
        //draw the player/villain
        g.drawAnimation(villain.getAnimation(), villain.getX(), villain.getY());  
        
        //draw flowers
        for(int i = 0; i < villain.getFlowersThrown().size(); i++){
            g.drawAnimation(villain.getFlowersThrown().get(i).getAnimation(), villain.getFlowersThrown().get(i).getX(), villain.getFlowersThrown().get(i).getY());
        }
        
        //draw npcs
        for (int i = 0; i < npcArray.size(); i++){
            NPC tempNpc = npcArray.get(i);
            //int npcHateMeter = (tempNpc.hateMeter / 100) * 42;
            
            g.drawAnimation(tempNpc.getAnimation(), tempNpc.getX(), tempNpc.getY());
            g.setColor(Color.green);
            g.fillRect(tempNpc.getX(), tempNpc.getY() - 10, tempNpc.hateMeter / 2, 5);
        } 
        
        //draw stones
        for (int i = 0; i < npcArray.size(); i++){
            NPC tempNpc = npcArray.get(i);
            for(int j = 0; j < tempNpc.getStonesThrown().size(); j++){
                
                if(tempNpc.stateNonStat == 1)
                    g.drawAnimation(tempNpc.getStonesThrown().get(j).getAnimation(), tempNpc.getStonesThrown().get(i).getX(), tempNpc.getStonesThrown().get(i).getY());
            }
        }
        
        g.setColor(Color.yellow);
        
        //draw hud components
        camera.untranslateGraphics();
        g.drawString("Health: ", 10, 10);
        g.drawString("Villain-o-meter: ", 10, 30);
        g.drawString("Flower power: ", 10, 50);
        
        
        
        double hD = ((double) villain.getHealth() / (double)100) * 300;
        int hI = (int) hD;
        double vMD = ((double) villain.getVMeter() / (double)100) * 300;
        int vI = (int) vMD;
        double flowerD = ((double) villain.getFlowerPower() / (double)100) * 300;
        int fI = (int) flowerD;
        g.setColor(Color.green);
        g.fillRect(160, 18, hI, 5);
        g.setColor(Color.red);
        g.fillRect(160, 38, vI, 5);
        g.setColor(Color.magenta);
        g.fillRect(160, 58, fI, 5);
        //g.draw(villain.villainPoly);
        
        
        
    }

    @Override
    public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
        
        //villain movement
        if (container.getInput().isKeyDown(Input.KEY_LEFT)){
            villain.left();
            
            if(villainCollisionCheck()){ 
                villain.right();
            }                  
        }
        
        if (container.getInput().isKeyDown(Input.KEY_RIGHT)){
            villain.right();
            //if collision to the right then move left
            if(villainCollisionCheck()){ 
                villain.left();
            }  
        }
        
        if (container.getInput().isKeyDown(Input.KEY_UP)){
            villain.up();
            //if collision above then move down
            if(villainCollisionCheck()){ 
                villain.down();
            }  

        }
        
        if (container.getInput().isKeyDown(Input.KEY_DOWN)){
            villain.down();
            //if collision to below then move up
            if(villainCollisionCheck()){ 
                villain.up();
            }
        }
        
        if (container.getInput().isKeyPressed(Input.KEY_SPACE)){
            villain.throwFlower();
            //play flower thrown sound
        }
        
        camera.centerOn(villain.getX(), villain.getY());
        villain.updateFlowers();
        villain.vMeter = updateVillainMeter();
        
        for (int i = 0; i < npcArray.size(); i++){
            NPC tempNpc = npcArray.get(i); 
            tempNpc.update();
            tempNpc.updateStones();
        }
        
        if(flowerCollisionCheck()){       
            //play a flower collision sound
            //add smash animation at this point
        }
        
        if(villainAggroCheck()){
            //play sound
            //npc start attacking
            for (int i = 0; i < npcArray.size(); i++){
                NPC tempNpc = npcArray.get(i);              
                if(tempNpc.stateNonStat == 2){
                    //happy npc!
                }
                else if(tempNpc.stateNonStat == 1){
                    //angry npc!
                    //System.out.println("checking");
                    for(int j = 0; j < villain.getFlowersThrown().size(); j++){
                       // check for intersection with map objects
                        //System.out.println("checking");
                        //if(villain.getFlowersThrown().get(j).flowerPoly.intersects(tempNpc.npcPoly)){
                            //villain.flowersThrown.remove(j);
                            //tempNpc.hateMeter = tempNpc.hateMeter - 10;
                            //flowerHit.play();
                        //}             
                    }
                    
                    //tempNpc.throwStone(villain);
                    //System.out.println(tempNpc.getStonesThrown().size());
                }
                
            } 
            
        }
        
        if(flowerHitNpcCheck()){
            
            flowerHit.play();
                        //play sound
        }
//        if(flowerHitNpcCheck()){
//            System.out.println("checking");
//                flowerHit.play();
//                //play sound
//            }
        
        //if(flowerHitNpcCheck()){
          //  flowerHit.play();
            //play sound
        //}
        
        if(villain.getVMeter() == 0){
            sbg.enterState(Game.LEVELFINISHSTATE);
        }
    }
    

    public boolean villainCollisionCheck() throws SlickException {
        for (int i = 0; i < BlockMap.entities.size(); i++){
            Block entity1 = (Block) BlockMap.entities.get(i);
            if (villain.villainPoly.intersects(entity1.poly)) {
                    return true;
            }  
                    
        }       
	return false;
    }
    
    public boolean flowerCollisionCheck(){
        for (int i = 0; i < BlockMap.entities.size(); i++){
            Block entity1 = (Block) BlockMap.entities.get(i);
            
            for(int j = 0; j < villain.getFlowersThrown().size(); j++){
                //check for intersection with map objects
                if(villain.getFlowersThrown().get(j).flowerPoly.intersects(entity1.poly)){
                    villain.flowersThrown.remove(j);
                    return true;
                }             
            }
            
        }       
	return false;
    }

    public boolean flowerHitNpcCheck(){
        for (int i = 0; i < npcArray.size(); i++){
            NPC tempNpc = npcArray.get(i);
            
            for(int j = 0; j < villain.getFlowersThrown().size(); j++){
                //check for intersection with map objects
                
                if(villain.getFlowersThrown().get(j).flowerPoly.intersects(tempNpc.npcPoly)){
                    //System.out.println("intersect");
                    villain.flowersThrown.remove(j);
                    if(tempNpc.hateMeter > 0){
                        tempNpc.hateMeter = tempNpc.hateMeter - 10;
                    }
                    return true;
                }             
            }
            
        }       
	return false;
    }
    
    public boolean villainAggroCheck(){
        for (int i = 0; i < npcArray.size(); i++){
            NPC tempNpc = npcArray.get(i);
        
            if(tempNpc.state != 2){
                if(villain.villainPoly.intersects(tempNpc.npcAggro)){
                    tempNpc.changeState(1);
                    return true;
                }
                else{            
                    tempNpc.changeState(0);
                }
            }
        }
	return false;
    }
    
    public int updateVillainMeter(){
        int meter = 100;
        
        for (int i = 0; i < npcArray.size(); i++){
            NPC tempNpc = npcArray.get(i);
            //System.out.println(tempNpc.state);
            if(tempNpc.stateNonStat == 2){
                meter = meter - 20;
            }                                   
        }  
        return meter;
    }
    
     /**
     * Class to handle recharging of character
     */
    private static class MoreFlowerPowerTask extends TimerTask{

        @Override
        public void run() {
            
            villain.addFlowerPower(20);
            if(villain.getFlowerPower() > 100)
                villain.flowerPower = 100;
                       
        }
    }
    
    
}
