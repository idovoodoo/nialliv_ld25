/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ld25;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Vector2f;

/**
 *
 * @author Steve
 */
public class NPC {
    //private vars
    private Animation npcUp;
    private Animation npcDown;
    private Animation npcLeft;
    private Animation npcRight;    
    private int xPos;
    private int yPos;
    private int facing = 3; //1 = up, 2 = right, 3 = down, 4 = left
    private int throwSpeed = 5;
    private int aggroRange = 155;
    private boolean playHappy = false;
    
    //public vars
    public Polygon npcPoly;  
    public Polygon npcAggro;
    public int hateMeter;
    public int damage;
    public int type = 1; //1 = boy1, 2 = boy2, 3 = girl1, 4 = girl3
    public static int state = 0; //0= idle, 1= attack
    public int stateNonStat = 0;
    private Sound happyFx;
    private Timer timer;
    public Vector2f stoneVector;
    public static boolean okayToThrow = false;
    public ArrayList<Stone> stonesThrown; 
    
    public NPC(int x, int y, int type, int damage){
        this.xPos = x;
        this.yPos = y;
        this.type = type;
        this.damage = damage;
        this.hateMeter = 100;
    }
    
    public void init() throws SlickException{
        //no animation for these yet so just placeholder like stills using
        //the same spritesheet
        SpriteSheet npc1 = null;
        SpriteSheet npc2 = null;
        SpriteSheet npc3 = null;
        SpriteSheet npc4 = null;
        
        if(this.type == 1){
            npc1 = new SpriteSheet("data/npc-boy1-up.png", 42, 42);
            npc2 = new SpriteSheet("data/npc-boy1-down.png", 42, 42);
            npc3 = new SpriteSheet("data/npc-boy1-left.png", 42, 42);
            npc4 = new SpriteSheet("data/npc-boy1-right.png", 42, 42);
        }
        
        npcUp = new Animation();
        npcDown = new Animation();
        npcLeft = new Animation();
        npcRight = new Animation();
        
        npcUp.setAutoUpdate(true);
        npcDown.setAutoUpdate(true);
        npcLeft.setAutoUpdate(true);
        npcRight.setAutoUpdate(true);
        
        for(int frame = 0; frame < 1; frame++)
            npcUp.addFrame(npc1.getSprite(frame, 0), 150);
        for(int frame = 0; frame < 1; frame++)
            npcDown.addFrame(npc2.getSprite(frame, 0), 150);
        for(int frame = 0; frame < 1; frame++)
            npcLeft.addFrame(npc3.getSprite(frame, 0), 150);
        for(int frame = 0; frame < 1; frame++)
            npcRight.addFrame(npc4.getSprite(frame, 0), 150);
        
        npcPoly = new Polygon(new float[]{
				this.xPos,this.yPos,
				this.xPos+42,this.yPos,
				this.xPos+42,this.yPos+42,
				this.xPos,this.yPos+42
                            }); 
        
        npcAggro = new Polygon(new float[]{
				this.xPos - aggroRange,this.yPos - aggroRange,
				this.xPos+32 +aggroRange ,this.yPos - aggroRange,
				this.xPos+32 + aggroRange,this.yPos+32 + aggroRange,
				this.xPos - aggroRange,this.yPos+32 +aggroRange
                            }); 
        
        happyFx = new Sound("data/happy.wav");
        timer = new Timer();
        timer.schedule(new ThrowStoneTask(),0 , 1 * 500);
        stonesThrown = new ArrayList<>(); 
    }
    
    public void updateStones(){
        for(int i = 0; i < stonesThrown.size(); i++){
            if(stonesThrown.get(i).update()){
                //stonesThrown.remove(i);
            }
        }
    }
    
    /**
     * Get all active flowers 
     * @return 
     */
    public ArrayList<Stone> getStonesThrown(){
        return stonesThrown;
    }
    
    /**
     * Throw a stone!
     * @throws SlickException 
     */
    public void throwStone(Villain villain) throws SlickException{
        Stone stone = null;
        
        if(okayToThrow){
            //villain to left             
            if(this.xPos > villain.getX()){
                //villain below
                if(this.yPos < villain.getY()){

                    int x = this.xPos - villain.getX();
                    int y = villain.getY() - this.getY();

                    stoneVector = new Vector2f(Math.toDegrees(Math.tan(x / y)));

                    stone = new Stone(this.xPos, this.yPos, stoneVector, 10);
                    stone.init();
                    stonesThrown.add(stone);
                    okayToThrow = false;
                }
                //villain above
                else{

                }
            }
            //villain to right
            else{
                //villain above
                if(this.yPos > villain.getY()){

                }
                //villain below
                else{

                }
            }
        }
        //stoneVector = new Vector2f();
    }
    

    public Animation getAnimation(){
        Animation anim = null;
        
        switch(facing){
            case 0:
                break;
            case 1:
                anim = npcUp;               
                break;
            case 2:
                anim = npcRight;  
                break;
            case 3:
                anim = npcDown;  
                break;
            case 4:
                anim = npcLeft;  
                break;
        }       
        return anim;
    }
    
    public void changeState(int state){
        this.state = state;      
        this.stateNonStat = state;
    }
    
    public void update(){
        if(hateMeter == 0){
            this.state = 2;  
            this.stateNonStat = 2;
        }
        
        switch(this.stateNonStat){
            case 0:
                break;
            case 1:
                //System.out.println("ANGRY MAN!!!");
                break;
            case 2:
                if(!playHappy){
                    happyFx.play();
                    playHappy = true;
                }
                //System.out.println("Happy! I love you!");
                break;
        }
    }
    
    public int getX(){
        return this.xPos;
    }
    
    public int getY(){
        return this.yPos;
    }

    private static class ThrowStoneTask extends TimerTask {

        @Override
        public void run() {
            if(state == 1){
                okayToThrow = true;
                //System.out.println("throw a stone");              
            }
        }
    }
}
