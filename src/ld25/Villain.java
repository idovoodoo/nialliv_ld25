package ld25;

import java.util.ArrayList;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Polygon;

/**
 *
 * @author Steve
 */
public class Villain {
    
    //private vars
    private Animation villainUp;
    private Animation villainDown;
    private Animation villainLeft;
    private Animation villainRight;    
    private int xPos;
    private int yPos;
    private int facing = 3; //1 = up, 2 = right, 3 = down, 4 = left
    private int throwSpeed = 5;
    
    private boolean isMoving = false;
    
    //public vars
    public Polygon villainPoly;     
    public int health;
    public int vMeter;
    public int flowerPower;
    public int flowerCost = 40;
    
    public Sound throwFlowerFx = null;
    public Sound walkFx = null;
    
    public ArrayList<Flower> flowersThrown = new ArrayList<>(); 
    
    /**
     * Constructor!
     * @param x
     * @param y 
     */
    public Villain(int x, int y){
        this.xPos = x;
        this.yPos = y;
        health = 100;
        vMeter = 100;
        flowerPower = 100;
    }
    /**
     * Do all initialisation for this class
     * @throws SlickException 
     */
    public void init() throws SlickException{
        SpriteSheet villain1 = new SpriteSheet("data/villain-up.png", 42, 42);
        SpriteSheet villain2 = new SpriteSheet("data/villain-down.png", 42, 42);
        SpriteSheet villain3 = new SpriteSheet("data/villain-left.png", 42, 42);
        SpriteSheet villain4 = new SpriteSheet("data/villain-right.png", 42, 42);

        villainUp = new Animation();
        villainDown = new Animation();
        villainLeft = new Animation();
        villainRight = new Animation();
        
        villainUp.setAutoUpdate(true);
        villainDown.setAutoUpdate(true);
        villainLeft.setAutoUpdate(true);
        villainRight.setAutoUpdate(true);
        
        
        for(int frame = 0; frame < 3; frame++)
            villainUp.addFrame(villain1.getSprite(frame, 0), 150);
        
        for(int frame = 0; frame < 3; frame++)
            villainDown.addFrame(villain2.getSprite(frame, 0), 150);            
        
        for(int frame = 0; frame < 3; frame++)
            villainLeft.addFrame(villain3.getSprite(frame, 0), 150);
        
        for(int frame = 0; frame < 3; frame++)
            villainRight.addFrame(villain4.getSprite(frame, 0), 150);
        
        villainPoly = new Polygon(new float[]{
				this.xPos,this.yPos,
				this.xPos+48,this.yPos,
				this.xPos+48,this.yPos+48,
				this.xPos,this.yPos+48
                            });     
        
        throwFlowerFx = new Sound("data/throwflower.wav");
        walkFx = new Sound("data/walk.wav");
    }
    
    /**
     * Update flower position
     */
    public void updateFlowers(){
        for(int i = 0; i < flowersThrown.size(); i++){
            if(flowersThrown.get(i).update()){
                flowersThrown.remove(i);
            }
        }
    }
    
    /**
     * Get all active flowers 
     * @return 
     */
    public ArrayList<Flower> getFlowersThrown(){
        return flowersThrown;
    }
    
    /**
     * Throw a flower!
     * @throws SlickException 
     */
    public void throwFlower() throws SlickException{
        Flower flower = null;
        
        if(flowerPower > 39){
            switch(facing){
                case 0:
                    break;
                case 1:
                    flower = new Flower(this.xPos + 15, this.yPos - 5, this.facing, 10);
                    break;
                case 2:
                    flower = new Flower(this.xPos + 25, this.yPos + 15, this.facing, 10);
                    break;
                case 3:
                    flower = new Flower(this.xPos + 15, this.yPos + 20, this.facing, 10);
                    break;
                case 4:
                    flower = new Flower(this.xPos, this.yPos + 15, this.facing, 10);
                    break;
            }
          
            
            flower.init();
            throwFlowerFx.play();
            flowersThrown.add(flower);
            flowerPower = flowerPower - flowerCost;
        }
        else{
            //play out of power sound
        }
            
    }
    
    public int addFlowerPower(int power){
        if(flowerPower < 100)
            flowerPower = flowerPower + power;
        return flowerPower;
    }
    /**
     * Get current flower power
     * @return 
     */
    public int getFlowerPower(){
        return flowerPower;
    }
    
    /**
     * Get current villain-o-meter
     * @return 
     */
    public int getVMeter(){
        return vMeter;
    }
    
    /**
     * Make a delta +/- change to the villain-o-meter
     * @param change
     * @return 
     */
    public int changeVmeter(int change){
        vMeter = vMeter + change;
        return vMeter;
    }
    
    /**
     * Do damage to our villain!
     * @param damage
     * @return current health of villain
     */
    public int damageVillain(int damage){
        health = health - damage;        
        return health;
    }
    
    /**
     * Get current health
     * @return 
     */
    public int getHealth(){
        return health;
    }
    
    /**
     * Move villain left
     */
    public void left(){
        this.xPos--;
        villainPoly.setX(xPos); 
        facing = 4;
        isMoving = true;
    }
    
    /**
    * Move villain right
    */
    public void right(){
        this.xPos++;
        villainPoly.setX(xPos); 
        facing = 2;
        isMoving = true;
    }
    
    /**
    * Move villain up
    */
    public void up(){
        this.yPos--;
        villainPoly.setY(yPos); 
        facing = 1;
        isMoving = true;
    }
    
    /**
    * Move villain down
    */
    public void down(){
        this.yPos++;
        villainPoly.setY(yPos); 
        facing = 3;
        isMoving = true;
    }
    
    
    public void stopAnim(){
        villainUp.stop();
        villainDown.stop();
        villainLeft.stop();
        villainRight.stop();
    }
    
    /**
     * Get current villain animation
     * @return 
     */
    public Animation getAnimation(){
        Animation anim = null;
        
        switch(facing){
            case 0:
                break;
            case 1:
                anim = villainUp;               
                break;
            case 2:
                anim = villainRight;  
                break;
            case 3:
                anim = villainDown;  
                break;
            case 4:
                anim = villainLeft;  
                break;
        }       
        return anim;
    }
    
    public Animation animationUp(){
        return villainUp;
    }
    
    public Animation animationDown(){
        return villainDown;
    }
        
    public Animation animationLeft(){
        return villainLeft;
    }
        
    public Animation animationRight(){
        return villainRight;
    }
            
    public void setX(int x){
        this.xPos = x;
    }
    
    public void setY(int y){
        this.yPos = y;
    }
    
    public int getX(){
        return this.xPos;
    }
    
    public int getY(){
        return this.yPos;
    }
            
}
